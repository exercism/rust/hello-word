use hello_world;

#[test]
fn test_hello_world() {
    let phrase = "Hello, World!";
    let result = hello_world::say();
    assert_eq!(phrase, result);
}
