/// Gives a friendly hello!
///
/// Says "Hello, World!".
///
/// # Example
///
/// ```
/// use hello_world;
/// let phrase = hello_world::say();
/// // Print text to the console
/// println!("{}", phrase);
/// ```
pub fn say() -> &'static str {
    "Hello, World!"
}
